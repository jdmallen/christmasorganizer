﻿using System;
using System.Data.SqlTypes;

namespace JDMallen.ChristmasOrganizer.Data.Context.Models {
    public class DBAddress {
        public SqlInt32 ID { get; set; }
        public SqlString Address1 { get; set; }
        public SqlString Address2 { get; set; }
        public SqlString Address3 { get; set; }
        public SqlString City { get; set; }
        public SqlString State { get; set; }
        public SqlString ZIP { get; set; }
        public SqlString ZIP4 { get; set; }
        public SqlBoolean IsCurrent { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public DateTimeOffset DateModified { get; set; }
    }
}