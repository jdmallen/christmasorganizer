﻿using System;
using System.Data.SqlTypes;

namespace JDMallen.ChristmasOrganizer.Data.Context.Models {
    public class DBPhone {
        public SqlInt32 ID { get; set; }
        public SqlInt32 ContactID { get; set; }
        public SqlInt32 TypeID { get; set; }
        public SqlString Number { get; set; }
        public SqlBoolean IsCurrent { get; set; }
        public SqlString Comments { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public DateTimeOffset DateModified { get; set; }
    }
}