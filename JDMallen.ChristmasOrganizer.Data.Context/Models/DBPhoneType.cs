﻿using System;
using System.Data.SqlTypes;

namespace JDMallen.ChristmasOrganizer.Data.Context.Models {
    public class DBPhoneType {
        public SqlInt32 ID { get; set; }
        public SqlString TypeName { get; set; }
        public SqlString TypeDescription { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public DateTimeOffset DateModified { get; set; }
    }
}
