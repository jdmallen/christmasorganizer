﻿using System;
using System.Data.SqlTypes;

namespace JDMallen.ChristmasOrganizer.Data.Context.Models {
    public class DBContact {
        public SqlInt32 ID { get; set; }
        public SqlString FirstName { get; set; }
        public SqlString MiddleName { get; set; }
        public SqlString LastName { get; set; }
        public SqlString Nickname { get; set; }
        public SqlInt32 AddressID { get; set; }
        public SqlDateTime Birthday { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public DateTimeOffset DateModified { get; set; }
        public SqlString Comments { get; set; }
    }
}