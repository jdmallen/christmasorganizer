﻿using System.Drawing;
using System.IO;
using AutoMapper;
using JDMallen.ChristmasOrganizer.Data.Context.Models;
using JDMallen.ChristmasOrganizer.Data.Models;

namespace JDMallen.ChristmasOrganizer.Data.Context.Config {
    internal class AppMappings : Profile {
        protected override void Configure() {
            // ReSharper disable UnusedVariable
            IMappingExpression<DBSession, Session> sessionMapping =
                CreateMap<DBSession, Session>();

            IMappingExpression<DBAddress, Address> addressMapping =
                CreateMap<DBAddress, Address>();

            IMappingExpression<DBContact, Contact> contactMapping =
                CreateMap<DBContact, Contact>();
            // ReSharper restore UnusedVariable
        }

        public static Image ByteArrayToImage(byte[] byteArrayIn) {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }
}
