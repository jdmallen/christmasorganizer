﻿using JDMallen.ChristmasOrganizer.Data.Context.Repos;
using JDMallen.ChristmasOrganizer.Data.IRepos;
using Ninject.Modules;

namespace JDMallen.ChristmasOrganizer.Data.Context.Config {
    public class AppNinjectModule : NinjectModule {
        public override void Load() {
            DoBind<AppContext,AppContext>();
            DoBind<ISessionRepo, SessionRepo>();
            DoBind<IContactRepo, ContactRepo>();
            DoBind<IAddressRepo, AddressRepo>();
        }

        private void DoBind<TInterface, TImplementation>()
            where TImplementation : TInterface {
            Bind<TInterface>().To<TImplementation>().InThreadScope();
        }
    }
}
