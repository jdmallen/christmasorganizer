﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using JDMallen.ChristmasOrganizer.Data.Context.Config;
using JDMallen.ChristmasOrganizer.Data.Context.Models;
using JDMallen.ChristmasOrganizer.Data.IRepos;
using JDMallen.ChristmasOrganizer.Data.Models;
using JDMallen.MVC.Helpers;

namespace JDMallen.ChristmasOrganizer.Data.Context.Repos {
    public class ContactRepo : Repo<AppContext,Contact,int>, IContactRepo {
        public ContactRepo(AppContext context) : base(context) {}

        public override Contact Get(int id) {
            throw new NotImplementedException();
        }

        public List<Contact> GetAllContacts() {
            return
                Map<Contact>(
                    Context.MainDBContext.Query<DBContact>(
                        SQLQuery.SQLQuery.ReadAllContacts)).ToList();
        }
    }
}
