﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using JDMallen.ChristmasOrganizer.Data.Context.Config;
using JDMallen.ChristmasOrganizer.Data.Context.Models;
using JDMallen.ChristmasOrganizer.Data.IRepos;
using JDMallen.ChristmasOrganizer.Data.Models;
using JDMallen.MVC.Helpers;

namespace JDMallen.ChristmasOrganizer.Data.Context.Repos {
    public class AddressRepo: Repo<AppContext,Address,int>, IAddressRepo {
        public AddressRepo(AppContext context) : base(context) {}
        public override Address Get(int id) {
            throw new NotImplementedException();
        }

        public List<Address> GetAllAddresses() {
            return
                Map<Address>(
                    Context.MainDBContext.Query<DBAddress>(
                        SQLQuery.SQLQuery.ReadAllAddresses)).ToList();
        }
    }
}
