INSERT INTO [dbo].[Sessions] (
	[SessionID]
    ,[Email]
    ,[Username]
    ,[CreationDate]
    ,[ExpirationDate]
    ,[AdditionalInfo])
VALUES (
	@SessionID
    ,@Email
    ,@Username
    ,@CreationDate
    ,@ExpirationDate
    ,@AdditionalInfo)