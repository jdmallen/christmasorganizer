﻿SELECT TOP 50 *
FROM [dbo].[Users]
WHERE (UPPER(UserName) LIKE '%'+@searchTerm+'%' AND @option = 0) 
	OR (UPPER(Email) LIKE '%'+@searchTerm+'%' AND @option = 1)
	OR ((UPPER(FirstName) LIKE '%'+@searchTerm+'%' 
		OR UPPER(LastName) LIKE '%'+@searchTerm+'%') AND @option = 2)