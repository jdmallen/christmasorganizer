﻿INSERT INTO [dbo].[Users]
    ([UserName]
    ,[PasswordHash]
    ,[Email]
    ,[FirstName]
    ,[LastName])
OUTPUT Inserted.UserID
VALUES
    (@UserName
    ,@PasswordHash
    ,@Email
    ,@FirstName
    ,@LastName)
