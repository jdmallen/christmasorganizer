﻿using System.Collections.Generic;
using JDMallen.ChristmasOrganizer.Data.Models;
using JDMallen.MVC.Helpers;

namespace JDMallen.ChristmasOrganizer.Data.IRepos {
    public interface IAddressRepo:IRepo<Address,int> {
        List<Address> GetAllAddresses();
    }
}
