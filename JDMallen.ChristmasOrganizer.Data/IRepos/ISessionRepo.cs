﻿using System.Collections.Generic;
using JDMallen.ChristmasOrganizer.Data.Models;
using JDMallen.MVC.Helpers;

namespace JDMallen.ChristmasOrganizer.Data.IRepos {
    public interface ISessionRepo : IRepo<Session> {
        Session LogIn(string usernameOrEmail, string password);
        string LogOut(string usernameOrEmail);
        bool ValidateSession(string sessionID);
        List<Session> ListSessionsByUser(string usernameOrEmail);
    }
}
