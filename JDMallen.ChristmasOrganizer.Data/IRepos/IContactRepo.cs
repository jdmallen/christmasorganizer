﻿using System.Collections.Generic;
using JDMallen.ChristmasOrganizer.Data.Models;
using JDMallen.MVC.Helpers;

namespace JDMallen.ChristmasOrganizer.Data.IRepos {
    public interface IContactRepo : IRepo<Contact,int> {
        List<Contact> GetAllContacts();
    }
}
