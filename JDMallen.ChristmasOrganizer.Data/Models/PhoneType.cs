﻿using System;
using JDMallen.MVC.Helpers;

namespace JDMallen.ChristmasOrganizer.Data.Models {
    public class PhoneType:Ent<int> {
        public new int ID { get; set; }
        public string TypeName { get; set; }
        public string TypeDescription { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public DateTimeOffset DateModified { get; set; }
    }
}
