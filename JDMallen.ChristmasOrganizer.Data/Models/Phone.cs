﻿using System;
using JDMallen.MVC.Helpers;

namespace JDMallen.ChristmasOrganizer.Data.Models {
    public class Phone : Ent<int> {
        public new int ID { get; set; }
        public PhoneType Type { get; set; }
        public string Number { get; set; }
        public bool IsCurrent { get; set; }
        public string Comments { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public DateTimeOffset DateModified { get; set; }
    }
}
