﻿using System;
using JDMallen.MVC.Helpers;

namespace JDMallen.ChristmasOrganizer.Data.Models {
    public class Address : Ent<int> {
        public new int ID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public bool IsCurrent { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public DateTimeOffset DateModified { get; set; }
    }
}
