﻿using System;
using System.Collections.Generic;
using JDMallen.MVC.Helpers;

namespace JDMallen.ChristmasOrganizer.Data.Models {
    public class Contact : Ent<int> {
        public new int ID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Nickname { get; set; }
        public Address Address { get; set; }
        public List<Phone> PhoneNumbers { get; set; } 
        public DateTime? Birthday { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public DateTimeOffset DateModified { get; set; }
        public string Comments { get; set; }
    }
}
