﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace JDMallen.ChristmasOrganizer {
    /// <summary>
    /// Configures and registers the Web API
    /// </summary>
    public static class WebApiConfig {
        /// <summary>
        /// Register the API with the Global App
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config) {
            // http://www.asp.net/web-api/overview/security/enabling-cross-origin-requests-in-web-api
            // TODO: Restrict CORS to only the front-end
            EnableCorsAttribute cors =
                  new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}",
                new {id = RouteParameter.Optional});
        }
    }
}
