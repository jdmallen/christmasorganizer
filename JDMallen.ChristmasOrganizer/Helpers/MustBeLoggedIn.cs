﻿using System;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;
using JDMallen.ChristmasOrganizer.Data.Context.Config;
using JDMallen.ChristmasOrganizer.Data.Context.Repos;

namespace JDMallen.ChristmasOrganizer.Helpers {
    public class MustBeLoggedIn : AuthorizeAttribute {
        protected override bool IsAuthorized(HttpActionContext httpContext) {
            AuthenticationHeaderValue authHeader =
                httpContext.Request.Headers.Authorization;
            if (authHeader == null || authHeader.Scheme != "Basic" ||
                string.IsNullOrEmpty(authHeader.Parameter)) {
                return false;
            }
            Tuple<string, string> nameAndSession = ExtractUserNameAndSession(authHeader.Parameter);
            if (nameAndSession == null) {
                return false;
            }
            string sessionID = nameAndSession.Item2;
            SessionRepo repo = new SessionRepo(new AppContext());
            return repo.ValidateSession(sessionID);
        }

        private Tuple<string, string> ExtractUserNameAndSession(string param) {
            string decoded =
                Encoding.UTF8.GetString(Convert.FromBase64String(param));
            string[] parts = decoded.Split(':');
            return parts.Length == 2 ? Tuple.Create(parts[0], parts[1]) : null;
        }
    }
}