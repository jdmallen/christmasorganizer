using System;
using System.Reflection;

namespace JDMallen.ChristmasOrganizer.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}