﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JDMallen.ChristmasOrganizer.Data.IRepos;

namespace JDMallen.ChristmasOrganizer.Controllers {
    public class MasterController : Controller {
        protected IAddressRepo AddressRepo { get; set; }

        public MasterController(IAddressRepo addressRepo) {
            AddressRepo = addressRepo;
        }
    }
}