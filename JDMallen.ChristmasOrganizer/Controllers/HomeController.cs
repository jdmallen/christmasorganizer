﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JDMallen.ChristmasOrganizer.Data.IRepos;

namespace JDMallen.ChristmasOrganizer.Controllers {
    public class HomeController : MasterController {
        public HomeController(IAddressRepo addressRepo) : base(addressRepo) {}

        public ActionResult Index() {
            var x = AddressRepo.GetAllAddresses();
            return View(x);
        }
    }
}